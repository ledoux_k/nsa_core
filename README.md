**4G / 5G Vitual core deploy with kubernetes**

There i have tried to deploy 4G and 5G virtual core. Try to look at this Control plane server as well i'm trying to implement using [this plan](https://open5gs.org/open5gs/assets/images/Open5GS_CUPS-01.pdf) for a full observation.


---

## Install NSA core 

Firstly install [helm](https://open5gs.org/open5gs/docs/guide/02-building-open5gs-from-sources/) and [kubernetes (microk8s for example)](https://microk8s.io/)

Then run the following commands :

#### kubectl create namespace core5g

Change into the helm-chart folder and install the 5G core:

#### helm -n core5g install open5gs ./

---

## Configuration of vEPC and 5g core

the idea is to adapt 5g and 4g core together and materialize a gateway to link them such as PCRF or SGW-c

You can start with some basiclly by follow this [configuration](https://open5gs.org/open5gs/docs/guide/02-building-open5gs-from-sources/)

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

## Maintener

 Christopher Adigun <adigunca@amazon.com>
 Ledoux KOUAM   <njongssiesaie@gmail.com>